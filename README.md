# Pipeline_Design

Implemented 5 stage pipeline in VHDL with no dummy stages, with and without data enable controls on the input and output registers, and managed to stall the pipeline for various conditions including instruction cache miss and data cache miss.